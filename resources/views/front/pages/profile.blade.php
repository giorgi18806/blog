@extends('front.layouts.master')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div class="leave-comment mr0"><!--leave comment-->

                        <h3 class="text-uppercase">My profile</h3>
                        @if(Session::has('status'))
                            <div class="alert alert-success">{{ session('status') }}</div>
                        @else
                            <br>
                        @endif
                        <img src="{{ $user->getImage() }}" alt="" class="profile-image">
                        <form class="form-horizontal contact-form" role="form" method="post" action="{{ route('profile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Name" value="{{ $user->name }}">
                                    @error('name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Email" value="{{ $user->email }}">
                                    @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="password" name="password"
                                           placeholder="Password">
                                    @error('password') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="file" class="form-control" id="image" name="avatar">
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn send-btn">Update</button>

                        </form>
                    </div><!--end leave comment-->
                </div>
                @include('front.partials.sidebar')
            </div>
        </div>
    </div>
@endsection
