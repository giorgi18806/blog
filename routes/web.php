<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/post/{slug}', [HomeController::class, 'show'])->name('show');
Route::get('/tag/{slug}', [HomeController::class, 'tag'])->name('tag');
Route::get('/category/{slug}', [HomeController::class, 'category'])->name('category');


//auth
Route::group(['middleware' => 'auth'], function() {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::post('/comment', [CommentController::class, 'store'])->name('comment.store');
});

Route::group(['middleware' => 'guest'], function() {
    Route::get('/register', [AuthController::class, 'registerForm']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::get('/login', [AuthController::class, 'loginForm'])->name('login');
    Route::post('/login', [AuthController::class, 'login']);
});

//admin
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function() {
    Route:: get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/categories', CategoryController::class);
    Route::resource('/tags', TagController::class);
    Route::resource('/users', UserController::class);
    Route::resource('/posts', PostController::class);
    Route::get('/comments', [\App\Http\Controllers\Admin\CommentController::class, 'index'])->name('comments.index');
    Route::get('/comments/toggle/{id}', [\App\Http\Controllers\Admin\CommentController::class, 'toggle'])->name('comments.toggle');
    Route::get('/comments/toggle/{id}', [\App\Http\Controllers\Admin\CommentController::class, 'toggle'])->name('comments.toggle');
    Route::delete('/comments/{id}/destroy', [\App\Http\Controllers\Admin\CommentController::class, 'destroy'])->name('comments.destroy');
});
