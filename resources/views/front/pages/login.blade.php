@extends('front.layouts.master')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div class="leave-comment mr0"><!--leave comment-->
                        @if(Session::has('status'))
                            <div class="alert-danger alert">{{ session('status') }}</div>
                        @endif
                        <h3 class="text-uppercase">Login</h3>
                        <br>
                        <form class="form-horizontal contact-form" role="form" method="post" action="/login">
                            @csrf
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="email" name="email"
                                           placeholder="Email">
                                    @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="password">
                                    @error('password') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                </div>
                            </div>
                            <button type="submit" class="btn send-btn">Login</button>

                        </form>
                    </div><!--end leave comment-->
                </div>
                @include('front.partials.sidebar')
            </div>
        </div>
    </div>
@endsection
