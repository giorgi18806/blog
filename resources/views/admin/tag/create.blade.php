@extends('admin.layouts.main')

@section('title', 'Create New Tag')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create New Tag
                <small>pleasant words..</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                {{ Form::open(['route' => 'tags.store']) }}
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Tag</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Title" value="{{ old('title') }}" autofocus>
                                @error('title') <span class="text-danger error"><small>{{ $message }}</small></span> @enderror
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ route('tags.index') }}" class="btn btn-default">Back</a>
                        <button class="btn btn-success pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer-->
                {{ Form::close() }}
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
@endsection
