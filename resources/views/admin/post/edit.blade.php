@extends('admin.layouts.main')

@section('title', 'Edit Post')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Post
                <small>pleasant words..</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{ Form::open(['route' => ['posts.update', $post], 'files' => true, 'method' => 'put']) }}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Post</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Title..." value="{{ $post->title }}">
                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>

                        <div class="form-group">
                            <img src="{{ $post->getImage() }}" alt="{{ $post->title }}" width="200">
                            <label for="exampleInputFile">Image</label>
                            <input type="file" name="image" id="exampleInputFile">
                            @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            {{ Form::select('category_id',
                                $categories,
                                $post->getCategoryID(),
                                ['class' => 'form-control select2'])
                            }}
                            @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group">
                            <label>Теги</label>
                            {{ Form::select('tags[]',
                                $tags,
                                $selectedTags,
                                ['class' => 'form-control select2', 'multiple' => 'multiple', 'data-placeholder' => 'Select tags'])
                             }}
                            @error('tags') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <!-- Date -->
                        <div class="form-group">
                            <label>Date:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" name="date" id="datepicker" value="{{ $post->date }}">
                                @error('date') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <!-- /.input group -->
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('is_featured', '1', $post->is_featured, ['class' => 'minimal']) }}
                            </label>
                            <label>
                                Recommend
                            </label>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('status', '1', $post->status, ['class' => 'minimal']) }}
                            </label>
                            <label>
                                Draft
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="description" id="content" cols="30" rows="10" class="form-control">{{ $post->description }}</textarea>
                            @error('description') <span class="text-danger error"><small>{{ $message }}</small></span> @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Full text</label>
                            <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ $post->content }}</textarea>
                            @error('content') <span class="text-danger error"><small>{{ $message }}</small></span> @enderror
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                    <button class="btn btn-success pull-right">Submit</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
@endsection
