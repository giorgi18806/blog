@extends('admin.layouts.main')

@section('title', 'Edit User')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit User
                <small>приятные слова..</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{ Form::open(['route' => ['users.update', $user], 'method' => 'put', 'files' => true]) }}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit User</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Name..." value="{{ $user->name }}">
                            @error('name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input type="text" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email..." value="{{ $user->email }}">
                            @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="text" class="form-control" name="password" id="exampleInputEmail1" placeholder="Password...">
                            @error('password') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group">
                            <img src="{{ $user->getImage() }}" alt="" width="200" class="img-responsive">
                            <label for="exampleInputFile">Avatar</label>
                            <input type="file" name="avatar" id="exampleInputFile">
                            @error('avatar') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('users.index') }}" class="btn btn-default">Back</a>
                    <button class="btn btn-success pull-right">Submit</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
@endsection
