<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\User;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('front.pages.profile', compact('user'));
    }

    public function update(Request $request )
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore(Auth::user()->id),
            ],
            'avatar' => 'nullable|mimes:jpeg,bmp,png,jpg,jfif',
        ]);

        $user = User::find(Auth::id());
        $user->edit($request->input());
        $user->generatePassword($request->get('password'));
        $user->uploadAvatar($request->file('avatar'));

        return back()->with('status', 'Profile has been updated successfully');
    }
}
