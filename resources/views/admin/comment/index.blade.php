@extends('admin.layouts.main')

@section('title', 'Comments')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
{{--                <li><a href="#">Examples</a></li>--}}
                <li class="active">Comments</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Comments list</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="create.html" class="btn btn-success">Add</a>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Text</th>
                            <th>Post</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($comments as $comment)
                                <tr>
                                    <td>{{ $comment->id }}</td>
                                    <td>{{ $comment->text }}</td>
                                    <td>{{ $comment->post->title }}</td>
                                    <td>
                                        @if($comment->status == 1)
                                            <a href="{{ route('comments.toggle', $comment->id) }}" class="fa fa-lock"></a>
                                        @else
                                            <a href="{{ route('comments.toggle', $comment->id) }}" class="fa fa-thumbs-o-up"></a>
                                        @endif
                                            {{ Form::open(['route' => ['comments.destroy', $comment->id], 'method' => 'delete']) }}
                                            <button type="submit" class="delete" onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
@endsection
